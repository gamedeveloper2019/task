#include <asio.hpp>
#include <iostream>
#include <memory>
#include <random>
#include "NetString.h"

class Client
{
public:
	Client(int pPort):
		mPort(pPort),
		mIsConnected(false),
		mSocket(mService)
	{
	}

	void connect()
	{
		try
		{
			mSocket.connect(asio::ip::tcp::endpoint(asio::ip::address::from_string("127.0.0.1"), mPort));
			std::cout << "Connect" << std::endl;
			mIsConnected = true;
		}
		catch (std::exception& pException)
		{
			mIsConnected = false;
			std::cout << pException.what() << std::endl;
		}
	}

	void writeAndRead(const std::string & pMessage)
	{
		try
		{
			if (mIsConnected)
			{
				std::string message = createNetString(pMessage);
				asio::write(mSocket, asio::buffer(message));

				std::cout << message << std::endl;

				asio::streambuf response;
				asio::error_code errorCode;
				size_t responseSize = asio::read(mSocket, response, errorCode);
				if (!errorCode || errorCode == asio::error::eof)
				{
					const char* data = asio::buffer_cast<const char*>(response.data());
					std::cout << data << std::endl;
				}
			}
		}
		catch (std::exception & pException)
		{
			std::cout << pException.what() << std::endl;
		}
	}

private:
	int mPort;
	bool mIsConnected;
	asio::io_service mService;
	asio::ip::tcp::socket mSocket;
};

thread_local std::mt19937 randomGenerator{ std::random_device{}() };

int random(int pMin, int pMax) 
{
	return std::uniform_int_distribution<int>{pMin, pMax}(randomGenerator);
}

std::string generateRandomKey()
{
	std::string key;
	int size = random(1, 1000);
	for (int j = 0; j < size; j++)
	{
		key += std::to_string(random(0, 9));
	}
	return key;
}

void test(int pPort)
{
	std::vector<std::unique_ptr<Client>> clients;
	for (int i = 0; i < 1000; i++)
	{
		auto client = std::make_unique<Client>(pPort);
		clients.push_back(std::move(client));
	}

	for (size_t i = 0; i < clients.size(); i++)
	{
		clients[i]->connect();
	}

	for (size_t i = 0; i < clients.size(); i++)
	{
		clients[i]->writeAndRead(std::to_string(i));
	}
}

int main(int pArgc, char** pArgv)
{
	int port = 555;
	if (pArgc > 1)
	{
		port = std::stoi(pArgv[1]);
	}

	Client client(port);
	client.connect();
	client.writeAndRead(generateRandomKey());

	//multiple connection test, maybe it will be useful
	//test(port);

	return 0;
}