#include <asio.hpp>
#include <iostream>
#include <set>
#include <memory>
#include <thread>
#include <chrono>
#include "NetString.h"

using namespace std::chrono_literals;

std::string findLongestPrefix(const std::set<std::string>& pPrefixes, const std::string& pKey)
{
	auto it = pPrefixes.lower_bound(pKey);

	if (it != pPrefixes.end() && *it == pKey)
	{
		return pKey;
	}
	auto reverseIt = std::make_reverse_iterator(it);
	while (reverseIt != pPrefixes.rend())
	{
		size_t i = 0;
		for (i = 0; i < (*reverseIt).size(); i++)
		{
			if ((*reverseIt).at(i) != pKey.at(i))
			{
				break;
			}
		}

		if (i == (*reverseIt).size())
		{
			return *reverseIt;
		}
		else if (i == 0)
		{
			return "";
		}
		reverseIt++;
	}
	return "";
}

class Connection
{
public:
	Connection(asio::io_service & pService):
		mSocket(pService)
	{
		mReadBuffer = asio::buffer(new char[MAX_MESSAGE_LENGTH], MAX_MESSAGE_LENGTH);
	}

	~Connection()
	{
		if (mReadBuffer.size() != 0)
		{
			delete [] mReadBuffer.data();
		}
	}

	asio::ip::tcp::socket& getSocket()
	{
		return mSocket;
	}

	asio::mutable_buffer& getReadBuffer()
	{
		return mReadBuffer;
	}
private:
	asio::ip::tcp::socket mSocket;
	asio::mutable_buffer mReadBuffer;
	const int MAX_MESSAGE_LENGTH = 1024;
};

class Server
{
public:
	Server(int pPort): 
		mPort(pPort), 
		mAcceptor(mService, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), mPort)),
		mRemovedMessage(createNetString("REMOVED")),
		mStoredMessage(createNetString("STORED"))
	{
	}

	void listen()
	{
		auto connection = std::make_shared<Connection>(mService);

		mAcceptor.async_accept(connection->getSocket(), [this, connection](auto pErrorCode) {
			if (!pErrorCode)
			{
				onClientConnect(connection);
				listen();
			}
			else
			{
				std::cout << "error " << pErrorCode.message() << std::endl;
			}
		});
	}

	void start()
	{
		std::cout << "Server started at port "<< mPort << std::endl;

		listen();
		mService.run();
	}

	void onClientConnect(std::shared_ptr<Connection> pConnection)
	{
		pConnection->getSocket().async_read_some(pConnection->getReadBuffer(),
			[pConnection, this](auto pErrorCode, auto pSize) {
				if (!pErrorCode)
				{
					const char* data = static_cast<const char*>(pConnection->getReadBuffer().data());
					std::string message(data, pSize);
					processMessage(pConnection, parseNetString(message));
				}
			});
	}

	void processMessage(std::shared_ptr<Connection> pConnection, const std::string & pMessage)
	{
		std::this_thread::sleep_for(500ms);

		std::string longestPrefix = findLongestPrefix(mPrefixes, pMessage);
		if (longestPrefix == "")
		{
			mPrefixes.insert(pMessage);

			asio::async_write(pConnection->getSocket(), asio::buffer(mStoredMessage.c_str(), mStoredMessage.size()),
				[pConnection](auto pErrorCode, auto pSize) {});
		}
		else
		{
			mPrefixes.erase(longestPrefix);

			asio::async_write(pConnection->getSocket(), asio::buffer(mRemovedMessage.c_str(), mRemovedMessage.size()),
				[pConnection](auto pErrorCode, auto pSize) {});
		}
	}

private:
	int mPort;
	asio::io_service mService;
	asio::ip::tcp::acceptor mAcceptor;
	std::set<std::string> mPrefixes;
	std::string mRemovedMessage;
	std::string mStoredMessage;
};

int main(int pArgc, char** pArgv)
{
	int port = 555;
	if (pArgc > 1)
	{
		port = std::stoi(pArgv[1]);
	}
	
	Server server(port);
	server.start();
	return 0;
}