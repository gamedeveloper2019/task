#include "NetString.h"

std::string createNetString(const std::string& pStr)
{
	return std::to_string(pStr.size()) + ":" + pStr + ",";
}

std::string parseNetString(const std::string& pNetStr)
{
	if (pNetStr.size() > 3)
	{
		auto pos = pNetStr.find(":");
		if (pos != std::string::npos)
		{
			auto size = std::stoi(pNetStr.substr(0, pos));
			if (size <= pNetStr.size() - 1)
			{
				return pNetStr.substr(pos+1, size);
			}
		}
	}
	return "";
}