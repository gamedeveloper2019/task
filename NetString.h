#pragma once
#include <string>

std::string createNetString(const std::string& pStr);
std::string parseNetString(const std::string& pNetStr);